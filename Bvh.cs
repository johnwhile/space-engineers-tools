﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Graphics;

namespace SpaceEngineers
{
    public class BVHPrimitive
    {
        // for other purpose
        public float area;
        public Vector3 centroid;
        public BoxAA volume;
        public int id;
        public Vector3 Center
        {
            get { return volume.Center; }
        }
        public override string ToString()
        {
            return "primitive " + id;
        }
    }

    public class BVHNode : IEnumerable<BVHNode>
    {
        private BVHTree main;

        public BVHNode parent, left, right;

        /// <summary>
        /// BoxAA use max and min value and is more faster in merging operations than BoxAA2 (center + halfsize version)
        /// </summary>
        public BoxAA bound;
        
        /// <summary>
        /// Only for test, i need to change with start-end index
        /// </summary>
        public List<BVHPrimitive> primitives = new List<BVHPrimitive>();
        
        /// <summary>
        /// root = DEPTH-1 to leaf = 0, negative to invalidate
        /// </summary>
        public int level = -1;

        /// <summary>
        /// debug
        /// </summary>
        public string info = "<none>";

        /// <summary>
        /// tree do array indexing
        /// <para>       A     </para>
        /// <para>     B   C   </para>
        /// <para>  D  E   F  G </para>
        /// </summary>
        /// <remarks>
        /// </remarks>
        public int IndexByLevel = -1;
        
        /// <summary>
        /// tree do array indexing
        /// <para>       A       </para>
        /// <para>     B   E     </para>
        /// <para>  C  D   F  G   </para>
        /// </summary>
        public int IndexByRelation = -1;

        /// <summary>
        /// Get next node using byrelation indexing
        /// </summary>
        public BVHNode next
        {
            get
            {
                // next of root doesnt' exist
                if (parent == null) return null;
                // always from left to right
                return parent.left == this ? parent.right : parent.next;
            }
        }
        
        /// <summary>
        /// next node index can be computed on fly.
        /// the offset is calculated as next.IndexByRelation - this.IndexByRelation and was 
        /// always >= 1
        /// </summary>
        public int escapeIndex
        {
            get 
            {
                if (next == null) return main.BVHNodeCOUNT;
                return next.IndexByRelation;
            }
        }
        /// <summary>
        /// if isn't leaf, both left and right node are not null, else something wrong on splitting
        /// </summary>
        public bool IsLeaf
        { 
            get { return left == null || right == null; }
        }

        /// <summary>
        /// </summary>
        public BVHNode(BVHTree main)
        {
            this.main = main;
            parent = null;
            left = null;
            right = null;
            IndexByLevel = -1;
            IndexByRelation = -1;
        }
        /// <summary>
        /// Not very efficent for many many many nodes, i will use Enumerator with non-recursive stack algorithm
        /// </summary>
        public IEnumerator<BVHNode> GetEnumerator()
        {
            if (!IsLeaf) 
            {
                yield return left;
                foreach (BVHNode node in left) yield return node;
                yield return right;
                foreach (BVHNode node in right) yield return node;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public override string ToString()
        {
            int offsetIndex = escapeIndex - this.IndexByRelation;

            string str = "node id:" + IndexByRelation + " -> esc:" + offsetIndex;
            foreach (BVHPrimitive obj in primitives) str += " " + obj.id;
            return str;
        }

        public TreeNode GetTreeRappresentation()
        {
            TreeNode node = new TreeNode(this.ToString());

            if (!IsLeaf)
            {
                node.Nodes.Add(left.GetTreeRappresentation());
                node.Nodes.Add(right.GetTreeRappresentation());
            }
            else
            {
                string str = "";
                foreach (BVHPrimitive obj in primitives) str += " " + obj.id;
                node.Text += str;
            }

            return node;
        }


    }

    // TODO : optimize internal primitives sorting using one list class instad building many instances
    public class BVHTree : IEnumerable<BVHNode>
    {
        int MAXOBJ = 6;
        int MAXDEPTH = 10;

        public int BVHNodeCOUNT = 0;

        public BVHNode root;
        public BoxAA rootvolume;
        public List<BVHPrimitive> primitives;
        public int counter_byrelation = 0;
        public int counter_bylevel = 0;

        public BVHTree(IList<Vector3> vertices, IList<Face16> triangles , int MaxPrimitivePerLeaf , int MaxTreeDepth)
        {
            MAXOBJ = MaxPrimitivePerLeaf;
            MAXDEPTH = MaxTreeDepth;

            Vector3[] tmp_tri = new Vector3[3];

            primitives = new List<BVHPrimitive>(triangles.Count);

            for (int i = 0; i < triangles.Count; i++)
            {
                BVHPrimitive obj = new BVHPrimitive();
                Face16 face = triangles[i];

                tmp_tri[0] = vertices[triangles[i].I];
                tmp_tri[1] = vertices[triangles[i].J];
                tmp_tri[2] = vertices[triangles[i].K];

                obj.volume = BoxAA.FromData(tmp_tri);
                obj.area = Vector3.Cross(tmp_tri[1] - tmp_tri[0], tmp_tri[2] - tmp_tri[0]).Length * 0.5f;
                obj.centroid = (tmp_tri[0] + tmp_tri[1] + tmp_tri[2]) / 3.0f;
                obj.id = i;

                primitives.Add(obj);
            }

            root = new BVHNode(this);
            BVHNodeCOUNT++;
            root.IndexByLevel = counter_bylevel = 0;
            root.IndexByRelation = counter_byrelation = 0;
            root.primitives = primitives;
            root.level = MAXDEPTH - 1;

            BoxAA rootvolume = BuildSubTree(root);
            root.bound = rootvolume;
        }

        /// <summary>
        /// Do splitting and sorting calculation, return the bound size of current node
        /// </summary>
        public BoxAA BuildSubTree(BVHNode node)
        {
            Vector3 mean = Vector3.Zero;
            int N = node.primitives.Count;

            node.IndexByRelation = counter_byrelation++;

            // is leaf case , no more splitting, calculate true bound
            
            if (N <= MAXOBJ || node.level == 0)
            {
                BoxAA bound = BoxAA.NaN;
                foreach (BVHPrimitive obj in node.primitives)
                    bound.Merge(obj.volume);
                return bound;
            }


            foreach (BVHPrimitive obj in node.primitives)
                mean += obj.Center;
            mean /= N;

            Vector3 variance = Vector3.Zero;
            foreach (BVHPrimitive obj in node.primitives)
            {
                Vector3 center = obj.Center - mean;
                center = center * center;
                variance += center;
            }
            variance /= N;

            eAxis bestaxe = eAxis.None;

            if (variance.x > variance.y)
            {
                bestaxe = variance.x > variance.z ? eAxis.X : eAxis.Z;
            }
            else
            {
                bestaxe = variance.y > variance.z ? eAxis.Y : eAxis.Z;
            }


            node.left = new BVHNode(this);
            node.right = new BVHNode(this);

            BVHNodeCOUNT += 2;

            node.left.primitives = new List<BVHPrimitive>();
            node.right.primitives = new List<BVHPrimitive>();

            node.left.parent = node.right.parent = node;
            node.left.level = node.right.level = node.level - 1;

            float splitValue = mean[bestaxe];

            foreach (BVHPrimitive obj in node.primitives)
            {
                float value = obj.Center[bestaxe];
                if (value > splitValue)
                {
                    node.left.primitives.Add(obj);
                }
                else
                {
                    node.right.primitives.Add(obj);
                }
            }

            // the leaf condition mean node.objects.count > 6, check unbalanced case to avoid infinite splitting and force splitting
            // rangeBalancedIndices = 1 is too small and in worst case you can optain a tree with Depth = number of primitives.
            int rangeBalancedIndices = N / 3;

            bool unbalanced = node.right.primitives.Count < rangeBalancedIndices || node.left.primitives.Count < rangeBalancedIndices;


            if (unbalanced)
            {
                node.info = "unbalanced left = " + node.left.primitives.Count + " right = " + node.right.primitives.Count;

                List<BVHPrimitive> presorted = new List<BVHPrimitive>();
                presorted.AddRange(node.right.primitives);
                presorted.AddRange(node.left.primitives);

                node.left.primitives.Clear();
                node.right.primitives.Clear();

                int splitIndex = N / 2;

                for (int i = 0; i < splitIndex; i++)
                    node.left.primitives.Add(presorted[i]);

                for (int i = splitIndex; i < N; i++)
                    node.right.primitives.Add(presorted[i]);

            }
            else
            {
                node.info = "ok";
            }

            node.primitives.Clear();

            node.right.IndexByLevel = counter_bylevel++;
            node.left.IndexByLevel = counter_bylevel++;

            BoxAA leftbound = BuildSubTree(node.left);
            BoxAA rightbound = BuildSubTree(node.right);

            node.left.bound = leftbound;
            node.right.bound = rightbound;

            return leftbound + rightbound;
        }

        public IEnumerator<BVHNode> GetEnumerator()
        {
            if (root == null) yield break;
            
            yield return root;
            foreach (BVHNode node in root) yield return node;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }


        public BVHNode[] ToArray()
        {
            BVHNode[] array = new BVHNode[BVHNodeCOUNT];
            int i = 0;
            foreach (BVHNode node in this) array[i++] = node;

            //debug the escape index method
            foreach (BVHNode node in array)
            {
                int esc = node.escapeIndex;
                
                if (node.parent != null)
                {
                    //node case
                    if (esc < 1) throw new Exception();
                    BVHNode next = node.next;

                    if (esc < BVHNodeCOUNT && array[esc] != next) throw new Exception();
                }
                else
                {
                    // root case
                    if (esc != BVHNodeCOUNT) throw new Exception();
                }

            }

            return array;
        }
    }

}
