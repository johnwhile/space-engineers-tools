﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Engine.Maths;

namespace SpaceEngineers
{
    // Maxscript require a constructor for each struct

    public struct MwmIntParam
    {
        public string name;
        public Int32 value;

        public MwmIntParam(string name,int value)
        {
            this.name = name;
            this.value = value;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;
            value = file.ReadInt32();
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, name)) return false;
            file.Write(value);
            return true;
        }

        public override string ToString()
        {
            return string.Format("{0} : {1}", name, value);
        }

    }

    public struct MwmStringParam
    {
        public string name;
        public string value;

        public MwmStringParam(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;
            if (!MwmHelper.ReadString(file, out value)) return false;
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, name)) return false;
            if (!MwmHelper.WriteString(file, value)) return false;
            return true;
        }

        public override string ToString()
        {
            return string.Format("{0} : {1}", name, value);
        }

    }

    public struct MwmBvhNode
    {
        // can't access to constants in maxscript
        public static int MAX_INDICES_PER_NODE = 6;

        public int index_count;
        public int[] index;

        /// <summary>
        /// Attention, is in local quantized format (from -32767 to 32767) but stored
        /// as box for semplicity.
        ///  min and max value seam don't have sense
        /// </summary>
        public BoxAA quantized;


        public uint serializedBytesCount
        {
            get { return 4 + 6 * 2 + 4 * (uint)index.Length; }
        }

        ushort clamp(float f)
        {
            return f < 0 ? (ushort)0 : f > ushort.MaxValue ? ushort.MaxValue : (ushort)f;
        }


        public MwmBvhNode(int[] indices, BoxAA quantized)
        {
            if (indices == null || indices.Length == 0)
            {
                index_count = 1;
                index = new int[] { -1 };
            }
            else
            {
                index_count = indices.Length;
                index = indices;
            }
            this.quantized = quantized;

        }

        public bool ReadBin(BinaryReader file)
        {
            index_count = file.ReadInt32();
            if (index_count < 0 || index_count > MAX_INDICES_PER_NODE) return false;

            index = new int[index_count];
            for (int i = 0; i < index_count; i++) index[i] = file.ReadInt32();
            quantized = new BoxAA(
                file.ReadUInt16(), file.ReadUInt16(), file.ReadUInt16(),
                file.ReadUInt16(), file.ReadUInt16(), file.ReadUInt16());

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            file.Write(index.Length);
            foreach (int idx in index) file.Write(idx);
            file.Write(clamp(quantized.min.x));
            file.Write(clamp(quantized.min.y));
            file.Write(clamp(quantized.min.z));
            file.Write(clamp(quantized.max.x));
            file.Write(clamp(quantized.max.y));
            file.Write(clamp(quantized.max.z));
            return true;
        }
    }

    public struct MwmKeyframe
    {
        public float Time;
        public Vector3 Translation;
        public Quaternion Rotation;
        
        /// <summary>
        /// Rotation * Traslation or viceversa... to check
        /// </summary>
        public Matrix4 Tranformation
        {
            get { return Matrix4.Translating(Translation); }
            set { }
        }


        public MwmKeyframe(float time, Vector3 traslation, Quaternion rotation)
        {
            this.Time = time;
            this.Translation = traslation;
            this.Rotation = rotation;
        }


        public bool ReadBin(BinaryReader file)
        {
            Time = (float)file.ReadDouble();
            Rotation.x = file.ReadSingle();
            Rotation.y = file.ReadSingle();
            Rotation.z = file.ReadSingle();
            Rotation.w = file.ReadSingle();
            Translation.x = file.ReadSingle();
            Translation.y = file.ReadSingle();
            Translation.z = file.ReadSingle();
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            file.Write((double)Time);
            file.Write(Rotation.x);
            file.Write(Rotation.y);
            file.Write(Rotation.z);
            file.Write(Rotation.w);
            file.Write(Translation.x);
            file.Write(Translation.y);
            file.Write(Translation.z);
            return true;
        }
    }


    public static class MwmHelper
    {
        /// <summary>
        /// String reader, return false if read process fail
        /// </summary>
        public static bool ReadString(BinaryReader file, out string str)
        {
            int length = file.ReadByte();
            char[] chars = file.ReadChars(length);
            str = new string(chars);
            
            // check if a digit key
            for (int i = 0; i < length; i++)
            {
                if (chars[i] < 32 || chars[i] > 126 )
                {
                    if (chars[i] != '\n' && chars[i] != '\r') return false;
                }
            }
            return true;
        }
        /// <summary>
        /// String writer, masimum 255 chars, return false if write process fail
        /// </summary>
        public static bool WriteString(BinaryWriter file, string str)
        {
            int length = str.Length;
            if (length > 255) return false;
            file.Write((byte)length);
            file.Write(str.ToCharArray(0, length));
            return true;
        }

        
        /// <summary>
        /// i use colum major matrices (directx use row major) because respect my math knowledge
        /// </summary>
        public static bool ReadMatrix(BinaryReader file, out Matrix4 matrix)
        {
            matrix = new Matrix4(
                file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(),
                file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(),
                file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(),
                file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle());
            matrix.Traspose();
            return true;
        }
        /// <summary>
        /// String writer, maximum 255 chars, return false if write process fail
        /// </summary>
        public static bool WriteMatrix(BinaryWriter file, Matrix4 matrix)
        {
            matrix.Traspose();
            file.Write(matrix.m00); file.Write(matrix.m01); file.Write(matrix.m02); file.Write(matrix.m03);
            file.Write(matrix.m10); file.Write(matrix.m11); file.Write(matrix.m12); file.Write(matrix.m13);
            file.Write(matrix.m20); file.Write(matrix.m21); file.Write(matrix.m22); file.Write(matrix.m23);
            file.Write(matrix.m30); file.Write(matrix.m31); file.Write(matrix.m32); file.Write(matrix.m33);
            return true;
        }

        #region VectorsPacker (implemented also in my math library becuase is important for me)

        //uint packed pseudo code = wyyyyyyy yyyyyyyy zxxxxxxx xxxxxxxx 
        const uint n15x15y_wsign_mask = 0x80000000; // 10000000 00000000 00000000 00000000
        const uint n15x15y_zsign_mask = 0x00008000; // 00000000 00000000 10000000 00000000
        // or x mask
        const uint n15x15y_mask = 0x00007FFF;       // 00000000 00000000 01111111 11111111

        static uint clamp(uint value, uint min, uint max)
        {
            return value < min ? min : value > max ? max : value;
        }



        public static VectorHalf4 Position_4half(float x, float y, float z)
        {
            float w = Math.Min((float)Math.Floor((double)Math.Max(Math.Max(Math.Abs(x), Math.Abs(y)), Math.Abs(z))), 2048f);
            w = w > 0.0f ? 1.0f / w : 1.0f;
            return new VectorHalf4(w * x, w * y, w * z, w);
        }


        public static Vector3 Position_4half(VectorHalf4 packed)
        {
            return new Vector3(packed.x * packed.w, packed.y * packed.w, packed.z * packed.w);
        }

        public static UInt32 Normalized_15X16Y(float x, float y, float z)
        {
            UInt32 packed = 0;
            if (z > 0) packed |= n15x15y_zsign_mask;

            // convert [-1.0f,1.0f] to to [0,32767]
            x = (x + 1) * 0.5f * 32767.0f;
            // convert [-1.0f,1.0f] to to [0,65534]
            y = (y + 1) * 0.5f * 65534.0f;

            // fix
            uint _x = clamp((uint)x, 0, 32767);
            uint _y = clamp((uint)y, 0, 65534);

            // instead write first short x and second short y in file, i write integer.
            // when i read uint32 with LittleEndian _x become the smaller value, _y greater
            return packed | (_y << 16) | _x;
        }
        public static Vector3 Normalized_15X16Y(UInt32 packed)
        {
            Vector3 v = new Vector3();

            // convert [0,32767] to [0.0f,1.0f]
            v.x = (packed & n15x15y_mask) / 32767.0f;
            // convert [0,32767] to [0.0f,1.0f]
            v.y = (packed >> 16) / 65534.0f;

            // convert [0.0f,1.0f] to [-1.0f,1.0f]
            v.x = 2 * v.x - 1;
            v.y = 2 * v.y - 1;

            // compute z , example if x or y = 1 , magnitude is ~ 0 and sqrt return NaN
            v.z = 1 - v.x * v.x - v.y * v.y;
            v.z = v.z > float.Epsilon ? (float)Math.Sqrt(v.z) : 0.0f;

            if ((packed & n15x15y_zsign_mask) == 0) v.z *= -1;

            return v;
        }
        
        /// <summary>
        /// Used in SpaceEngineersGame for normalized vector
        /// z value are calculated from x y
        /// w can be 1 or -1
        /// </summary>
        public static UInt32 Normalized_15X15Y(float x, float y, float z, float w = 1)
        {
            UInt32 packed = 0;
            if (z > 0) packed |= n15x15y_zsign_mask;
            if (w > 0) packed |= n15x15y_wsign_mask;

            // convert [-1.0f,1.0f] to to [0,32767]
            x = (x + 1) * 0.5f * 32767.0f;
            y = (y + 1) * 0.5f * 32767.0f;

            // fix
            uint _x = clamp((uint)x, 0, 32767);
            uint _y = clamp((uint)y, 0, 32767);

            // instead write first short x and second short y in file, i write integer.
            // when i read uint32 with LittleEndian _x become the smaller value, _y greater
            return packed | (_y << 16) | _x;
        }
        /// <summary>
        /// Used in SpaceEngineersGame
        /// z value are calculated from x y
        /// w value can be 1 or -1
        /// </summary>
        public static void Normalized_15X15Y(UInt32 packed, out float x, out float y, out float z, out float w)
        {
            // convert [0,32767] to [0.0f,1.0f]
            x = (packed & n15x15y_mask) / 32767.0f;
            y = ((packed >> 16) & n15x15y_mask) / 32767.0f;

            // convert [0.0f,1.0f] to [-1.0f,1.0f]
            x = 2 * x - 1;
            y = 2 * y - 1;

            // compute z , example if x or y = 1 , magnitude is ~ 0 and sqrt return NaN
            z = 1 - x * x - y * y;
            z = z > float.Epsilon ? (float)Math.Sqrt(z) : 0.0f;

            if ((packed & n15x15y_zsign_mask) == 0) z *= -1;
            w = 1.0f;
            if ((packed & n15x15y_wsign_mask) == 0) w *= -1;
        }
        /// <summary>
        /// <seealso cref="VectorPacker.Normalized_15X15Y(UInt32,out float,out float,out float,out float)"/>
        /// </summary>
        public static Vector3 Normalized_15X15Y(UInt32 packed)
        {
            Vector3 vector = Vector3.Zero;
            float w;
            Normalized_15X15Y(packed, out vector.x, out vector.y, out vector.z, out w);
            return vector;
        }
        public static UInt32 Normalized_15X15Y(Vector3 v)
        {
            return Normalized_15X15Y(v.x, v.y, v.z, 1);
        }
        #endregion

    }

}
