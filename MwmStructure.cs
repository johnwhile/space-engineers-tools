﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Engine.Maths;
using Engine.Graphics;
using Engine.Geometry;

namespace SpaceEngineers
{
    // i read and store also array lenght counter for maxscript but when write i omited it and
    // i write it from array.lenght()

    public class MwmHeader
    {
        string title = "Debug";
        public int version = 01052002;

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title) || title != "Debug") return false;
            int debug_int = file.ReadInt32();

            if (debug_int > 0)
            {
                string versionstring;
                if (!MwmHelper.ReadString(file, out versionstring)) return false;
                versionstring = versionstring.Remove(0, 8);
                if (!int.TryParse(versionstring, out version)) return false;
            }
            return true;
        }

        public bool WriteBin(BinaryWriter file, int version)
        {
            if (!MwmHelper.WriteString(file, title)) return false;
            file.Write(1);

            if (1 > 0)
            {
                string versionstring = string.Format("Version:{0}", version.ToString("D8"));
                if (!MwmHelper.WriteString(file, versionstring)) return false;
            }
            return true;
        }
    }

    public class MwmDummies
    {
        string title = "Dummies";

        public int dummies_count = 0;
        public MwmDummy[] dummies;

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            dummies_count = file.ReadInt32();

            // safety check to avoid inconsistent data
            if (title != "Dummies" || dummies_count<0 || dummies_count > 1000) return false;

            dummies = new MwmDummy[dummies_count];
            for (int i = 0; i < dummies_count; i++)
            {
                dummies[i] = new MwmDummy();
                if (!dummies[i].ReadBin(file)) return false;
            }

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title)) return false;

            if (dummies != null)
            {
                file.Write(dummies.Length);
                for (int i = 0; i < dummies.Length; i++)
                {
                    if (!dummies[i].WriteBin(file)) return false;
                }
            }
            else file.Write(0);

            return true;
        }
    }
    
    public class MwmDummy
    {
        public string name = "";
        public Matrix4 transform = Matrix4.Identity;
        public int properties_count;
        public MwmStringParam[] properties;

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file,out name)) return false;
            if (!MwmHelper.ReadMatrix(file, out transform)) return false;
            properties_count = file.ReadInt32();

            // safety check to avoid inconsistent data
            if (properties_count < 0 || properties_count > 1000) return false;

            properties = new MwmStringParam[properties_count];
            for(int i = 0;i<properties_count;i++)
            {
                properties[i] = new MwmStringParam();
                if (!properties[i].ReadBin(file)) return false;
            }

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file,  name)) return false;
            if (!MwmHelper.WriteMatrix(file,  transform)) return false;
            file.Write(properties.Length);
            for (int i = 0; i < properties.Length; i++)
                if (!properties[i].WriteBin(file)) return false;

            return true;
        }
    }

    public class MwmVertices
    {
        public int position_count;
        public int normal_count;
        public int texcood_count;
        public int binormal_count;
        public int tangent_count;
        public int texchannel_count;

        public Vector3[] position;
        public Vector3[] normal;
        public Vector2[] texcood;
        public Vector4[] binormal;
        public Vector4[] tangent;
        public Vector2[] texchannel;

        byte clamp(float f,byte min,byte max){return f < min ? min : f > max ? max : (byte)f;}
        float abs(float f) { return f > 0 ? f : -f; }
        float min(float a, float b) { return a < b ? a : b; }
        int min(int a, int b) { return a < b ? a : b; }
        float max(float a, float b) { return a > b ? a : b; }
        float max(float a, float b, float c) { return a > b ? max(a, c) : max(b, c); }



        /// <summary>
        /// Generate a new vertices set, tbn matrix recalculate using currents normals.
        /// is not interessant customize Tangenst and Birnormals vectors.
        /// </summary>
        /// <param name="texcoord">if null generate a new set using a planar projection to XY plane</param>
        /// <param name="normals">if null generate a new set using vertices and triangles</param>
        public static MwmVertices Generate(IList<Vector3> vertices, IList<Vector2> texcoord, IList<Vector3> normals, IList<Face16> triangles)
        {
            MwmVertices structure = new MwmVertices();
            int numverts = vertices.Count;

            structure.position = new Vector3[numverts];
            vertices.CopyTo(structure.position, 0);

            if (texcoord == null)
            {
                structure.texcood = GeometryTools.GetPlanarProjection(vertices, Vector2.Zero, Vector2.One, Matrix4.Identity);
            }
            else
            {
                structure.texcood = new Vector2[numverts];
                texcoord.CopyTo(structure.texcood, 0);
            }

            if (normals == null)
            {
                structure.normal = GeometryTools.CalculateNormals(vertices, triangles);
            }
            else
            {
                structure.normal = new Vector3[numverts];
                normals.CopyTo(structure.normal, 0);
            }

            structure.tangent = GeometryTools.CalculateTangents(vertices, texcoord, normals, triangles);
            structure.binormal = new Vector4[numverts];

            for (int i = 0; i < numverts; i++)
            {
                structure.binormal[i] = (Vector4)Vector3.Cross(structure.normal[i], structure.tangent[i]); // w=1
                structure.binormal[i] *= structure.tangent[i].w;
            }

            return structure;
        }

        /// <summary>
        /// Return false also if param name not mach with hardcoded constants
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            string title;
            int count;

            for (int i = 0; i < 6; i++)
            {
                if (!MwmHelper.ReadString(file, out title)) return false;
                count = file.ReadInt32();
                // safety check to avoid inconsistent data
                if (count < 0 || count > 1000000) return false;

                switch (title)
                {
                    case "Vertices":
                        position_count = count;
                        if (count > 0)
                        {
                            position = new Vector3[count];
                            for (int j = 0; j < count; j++)
                            {
                                position[j].x = Half.ToFloat(file.ReadUInt16());
                                position[j].y = Half.ToFloat(file.ReadUInt16());
                                position[j].z = Half.ToFloat(file.ReadUInt16());
                                float w = Half.ToFloat(file.ReadUInt16());
                                position[j].x *= w;
                                position[j].y *= w;
                                position[j].z *= w;
                            }
                        }
                        break;
                    case "TexCoords0":
                        texcood_count = count;
                        if (count > 0)
                        {
                            texcood = new Vector2[count];
                            for (int j = 0; j < count; j++)
                            {
                                texcood[j].x = Half.ToFloat(file.ReadUInt16());
                                texcood[j].y = Half.ToFloat(file.ReadUInt16());
                            }
                        }
                        break;
                    case "Normals":
                        normal_count = count;
                        if (count > 0)
                        {
                            normal = new Vector3[count];
                            for (int j = 0; j < count; j++)
                                normal[j] = MwmHelper.Normalized_15X15Y(file.ReadUInt32());
                        }
                        break;
                    case "Binormals":
                        binormal_count = count;
                        if (count > 0)
                        {
                            binormal = new Vector4[count];
                            for (int j = 0; j < count; j++)
                                MwmHelper.Normalized_15X15Y(file.ReadUInt32(), out binormal[j].x, out binormal[j].y, out binormal[j].z, out binormal[j].w);
                        }
                        break;
                    case "Tangents":
                        tangent_count = count;
                        if (count > 0)
                        {
                            tangent = new Vector4[count];
                            for (int j = 0; j < count; j++)
                                MwmHelper.Normalized_15X15Y(file.ReadUInt32(), out tangent[j].x, out tangent[j].y, out tangent[j].z, out tangent[j].w);
                        }
                        break;
                    case "TexCoords1":
                        texchannel_count = count;
                        if (count > 0)
                        {
                            texchannel = new Vector2[count];
                            for (int j = 0; j < count; j++)
                            {
                                texchannel[j].x = Half.ToFloat(file.ReadUInt16());
                                texchannel[j].y = Half.ToFloat(file.ReadUInt16());
                            }
                        }
                        break;

                    default: 
                        return false;
                }
            }
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            int count;

            if (!MwmHelper.WriteString(file, "Vertices")) return false;
            
            if (position != null)
            {
                count = position.Length;
                file.Write(position.Length);
                               
                for (int i=0;i<count;i++)
                {
                    float w = max(abs(position[i].x), abs(position[i].y), abs(position[i].z));
                    // floor the w
                    int minw = (int)w;
                    // clamp to range [1,2048]
                    minw = minw < 1 ? 1 : minw > 2048 ? 2048 : minw;

                    VectorHalf4 hv = new VectorHalf4(position[i].x / minw, position[i].y / minw, position[i].z / minw, minw);
                    file.Write(Half.GetBits(hv.hx));
                    file.Write(Half.GetBits(hv.hy));
                    file.Write(Half.GetBits(hv.hz));
                    file.Write(Half.GetBits(hv.hw));
                }
            }
            else
            {
                count = 0;
                file.Write(0);
            }

            if (!MwmHelper.WriteString(file, "Normals")) return false;
            if (normal != null)
            {
                if (normal.Length > 0 && normal.Length != count) return false;
                file.Write(normal.Length);
                foreach (Vector3 v in normal)
                {
                    file.Write(MwmHelper.Normalized_15X15Y(v.x, v.y, v.z));
                }
            }
            else
            {
                file.Write(0);
            }

            if (!MwmHelper.WriteString(file, "TexCoords0")) return false;

            if (texcood != null)
            {
                if (texcood.Length > 0 && texcood.Length != count) return false;
                file.Write(texcood.Length);
                foreach (Vector2 v in texcood)
                {
                    VectorHalf2 hv = (VectorHalf2)v;
                    file.Write(Half.GetBits(hv.hx));
                    file.Write(Half.GetBits(hv.hy));
                }
            }
            else
            {
                file.Write(0);
            }



            if (!MwmHelper.WriteString(file, "Binormals")) return false;
            if (binormal != null)
            {
                if (binormal.Length > 0 && binormal.Length != count) return false;
                file.Write(binormal.Length);
                foreach (Vector4 v in binormal)
                {
                    file.Write(MwmHelper.Normalized_15X15Y(v.x, v.y, v.z, v.w));
                }
            }
            else
            {
                file.Write(0);
            }


            if (!MwmHelper.WriteString(file, "Tangents")) return false;
            if (tangent != null)
            {
                if (tangent.Length > 0 && tangent.Length != count) return false;
                file.Write(tangent.Length);
                foreach (Vector4 v in tangent)
                {
                    file.Write(MwmHelper.Normalized_15X15Y(v.x, v.y, v.z,v.w));
                }
            }
            else
            {
                file.Write(0);
            }

            if (!MwmHelper.WriteString(file, "TexCoords1")) return false;
            if (texchannel != null)
            {
                if (texchannel.Length > 0 && texchannel.Length != count) return false;
                file.Write(texchannel.Length);
                foreach (Vector2 v in texchannel)
                {
                    VectorHalf2 hv = (VectorHalf2)v;
                    file.Write(Half.GetBits(hv.hx));
                    file.Write(Half.GetBits(hv.hy));
                }
            }
            else
            {
                file.Write(0);
            }

            return true;
        }
    }
   
    public class MwmModelParams
    {
        const byte TRUE = 1;
        const byte FALSE = 0;


        public bool RescaleToLengthInMeters;
        public float LengthInMeters;
        public float RescaleFactor;
        public bool Centered;
        public bool UseChannelTextures;
        public float SpecularShininess;
        public float SpecularPower;
        public BoxAA BoundingBox;
        public Sphere BoundingSphere;
        public bool SwapWindingOrder;

        public MwmModelParams()
        {
            SetDefault();
        }
        /// <summary>
        /// Initialize with default parameters
        /// </summary>
        public void SetDefault()
        {
            RescaleToLengthInMeters = false;
            RescaleFactor = 0.01f;
            Centered = false;
            SpecularShininess = 0.8f;
            SpecularPower = 10.0f;
            BoundingBox = new BoxAA(Vector3.Zero, Vector3.Zero);
            BoundingSphere = new Sphere(Vector3.Zero, -1);
            SwapWindingOrder = false;
        }

        public static MwmModelParams Default()
        {
            return new MwmModelParams();
        }

        /// <summary>
        /// Return false also if param name not mach with hardcoded constants
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            string str;

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "RescaleToLengthInMeters") return false;
            RescaleToLengthInMeters = file.ReadByte() != 0;

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "LengthInMeters") return false;
            LengthInMeters = file.ReadSingle();

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "RescaleFactor") return false;
            RescaleFactor = file.ReadSingle();

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "Centered") return false;
            Centered = file.ReadByte() != 0;

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "UseChannelTextures") return false;
            UseChannelTextures = file.ReadByte() != 0;

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "SpecularShininess") return false;
            SpecularShininess = file.ReadSingle();

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "SpecularPower") return false;
            SpecularPower = file.ReadSingle();

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "BoundingBox") return false;
            BoundingBox = new BoxAA(file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle());

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "BoundingSphere") return false;
            BoundingSphere = new Sphere(file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle());

            if (!MwmHelper.ReadString(file, out str)) return false;
            if (str != "SwapWindingOrder") return false;
            SwapWindingOrder = file.ReadByte() != 0;

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, "RescaleToLengthInMeters")) return false;
            file.Write(RescaleToLengthInMeters ? TRUE : FALSE);

            if (!MwmHelper.WriteString(file, "LengthInMeters")) return false;
            file.Write(LengthInMeters);

            if (!MwmHelper.WriteString(file, "RescaleFactor")) return false;
            file.Write(RescaleFactor);

            if (!MwmHelper.WriteString(file, "Centered")) return false;
            file.Write(Centered ? TRUE : FALSE);

            if (!MwmHelper.WriteString(file, "UseChannelTextures")) return false;
            file.Write(UseChannelTextures ? TRUE : FALSE);

            if (!MwmHelper.WriteString(file, "SpecularShininess")) return false;
            file.Write(SpecularShininess);

            if (!MwmHelper.WriteString(file, "SpecularPower")) return false;
            file.Write(SpecularPower);

            if (!MwmHelper.WriteString(file, "BoundingBox")) return false;
            file.Write(BoundingBox.min.x);
            file.Write(BoundingBox.min.y);
            file.Write(BoundingBox.min.z);
            file.Write(BoundingBox.max.x);
            file.Write(BoundingBox.max.y);
            file.Write(BoundingBox.max.z);

            if (!MwmHelper.WriteString(file, "BoundingSphere")) return false;
            file.Write(BoundingSphere.center.x);
            file.Write(BoundingSphere.center.y);
            file.Write(BoundingSphere.center.z);
            file.Write(BoundingSphere.radius);

            if (!MwmHelper.WriteString(file, "SwapWindingOrder")) return false;
            file.Write(SwapWindingOrder ? TRUE : FALSE);
            
            return true;
        }
    }

    public class MwmParts
    {
        string title = "MeshParts";
        public int parts_count = 0;
        public MwmPart[] parts;

        public bool ReadBin(BinaryReader file, int version)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            if (title != "MeshParts") return false;
            parts_count = file.ReadInt32();
            if (parts_count < 0 || parts_count > 1000) return false;

            parts = new MwmPart[parts_count];
            for (int i = 0; i < parts_count; i++)
            {
                parts[i] = new MwmPart();
                if (!parts[i].ReadBin(file, 1052002)) return false;
            }

            return true;
        }

        public bool WriteBin(BinaryWriter file, int version)
        {
            if (!MwmHelper.WriteString(file, title)) return false;
            
            if (parts != null)
            {
                file.Write(parts.Length);
                foreach (MwmPart part in parts)
                    if (!part.WriteBin(file, 1052002)) return false;
            }
            else file.Write(0);

            return true;
        }
    }

    public class MwmPart
    {
        public int hash;
        public int triangle_count = 0;
        public Face16[] triangles;
        public bool hasMaterial = true;
        public MwmModelMaterial material = null;

        public bool ReadBin(BinaryReader file, int version)
        {
            hash = file.ReadInt32();
            if (version < 1052001) file.ReadInt32();

            triangle_count = file.ReadInt32();
            if (triangle_count < 0 || triangle_count > 1000000 || triangle_count % 3 != 0) return false;

            // only triangles indices supported now, is possible other directx primitives ? NO, BLAME CANADA !!!
            triangle_count /= 3;

            triangles = new Face16[triangle_count];
            for (int i = 0; i < triangle_count; i++)
            {
                int i0 = file.ReadInt32();
                int i1 = file.ReadInt32();
                int i2 = file.ReadInt32();
                
                // only a coerence test, i'm using 16 bit indices because i assume you will never create a block with 65535 vertices
                if (i0 < 0 || i0 > ushort.MaxValue) return false;
                if (i1 < 0 || i1 > ushort.MaxValue) return false;
                if (i2 < 0 || i2 > ushort.MaxValue) return false;

                triangles[i] = new Face16(i0, i1, i2);

            }
            hasMaterial = file.ReadByte() != 0;

            if (hasMaterial)
            {
                material = new MwmModelMaterial();
                if (!material.ReadBin(file, version)) return false;
            }


            return true;
        }

        public bool WriteBin(BinaryWriter file, int version)
        {
            file.Write(hash);
            if (version < 1052001) file.Write((int)0);

            if (triangles != null)
            {
                file.Write(triangles.Length*3);
                foreach (Face16 face in triangles)
                {
                    file.Write((int)face.I);
                    file.Write((int)face.J);
                    file.Write((int)face.K);
                }
            }
            else file.Write(0);

            if (material != null)
            {
                file.Write((byte)1);
                if (!material.WriteBin(file, version)) return false;
            }
            else
            {
                file.Write((byte)0);
            }
            return true;
        }
    }

    public class MwmModelMaterial 
    {
        public enum Technique
        {
            MESH = 0,
            VOXELS_DEBRIS = 1,
            VOXEL_MAP = 2,
            ALPHA_MASKED = 3,
            DECAL = 4,
            HOLO = 5,
            VOXEL_MAP_SINGLE = 6,
            VOXEL_MAP_MULTI = 7,
            SKINNED = 8,
            MESH_INSTANCED = 9,
            MESH_INSTANCED_SKINNED = 10,
            GLASS = 11,
            MESH_INSTANCED_GENERIC = 12,
            MESH_INSTANCED_GENERIC_MASKED = 13,
        }

        private static Dictionary<string, Technique> TechniqueEnumParser;
        
        static MwmModelMaterial()
        {
            TechniqueEnumParser = new Dictionary<string, Technique>();
            for (int i=0 ; i<= 13;i++) TechniqueEnumParser.Add(((Technique)i).ToString(),(Technique)i);
        }


        public static int instanceCounter = 0;

        public string name = "Default";
        public int texpath_count = 0;
        public MwmStringParam[] texpath;
        public float glossiness = 2;
        public Vector3 diffuseColor = Vector3.One;
        public Vector3 specularColor = Vector3.Zero;
        public Technique technique = Technique.MESH;

        public string GlassCW = "";
        public string GlassCCW = "";
        public bool GlassSmoothNormals = false;
        public Vector4 GlassExtra = Vector4.Zero;

        public MwmModelMaterial()
        {
            technique = Technique.MESH;
        }
        /// <summary>
        /// Can be load on maxscript
        /// </summary>
        public MwmModelMaterial(string name, string[] paths, string[] type)
            : this()
        {

        }

        public static MwmModelMaterial Default()
        {
            MwmModelMaterial mat = new MwmModelMaterial();
            mat.name = "Default - " + (instanceCounter++);
            mat.diffuseColor = Vector3.One;
            mat.specularColor = Vector3.Zero;
            mat.glossiness = 2;
            mat.texpath = new MwmStringParam[]
            {
                new MwmStringParam {name = "DiffuseTexture" , value = @"Textures\Models\Cubes\ColoringTexture_me.dds" },
                new MwmStringParam {name = "NormalTexture" , value = "" },
            };

            mat.technique = Technique.MESH;
            return mat;
        }

        public bool ReadBin(BinaryReader file, int version)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;

            if (version < 1052002)
            {
                texpath_count = 2;
            }
            else
            {
                texpath_count = file.ReadInt32();
                if (texpath_count < 0 || texpath_count > 1000) return false;
            }

            texpath = new MwmStringParam[texpath_count];
            for (int i = 0; i < texpath_count; i++)
                if (!texpath[i].ReadBin(file)) return false;

            glossiness = file.ReadSingle();
            diffuseColor.x = file.ReadSingle();
            diffuseColor.y = file.ReadSingle();
            diffuseColor.z = file.ReadSingle();
            specularColor.x = file.ReadSingle();
            specularColor.y = file.ReadSingle();
            specularColor.z = file.ReadSingle();

           
            if (version >= 1052001)
            { 
                string str;
                if (!MwmHelper.ReadString(file, out str)) return false;
                if (!TechniqueEnumParser.TryGetValue(str, out technique)) return false;
            }
            else
            {
                technique = (Technique)file.ReadInt32();
            }

            if (technique == Technique.GLASS)
            {
                if (version < 1043001)
                {

                }
                else
                {
                    GlassExtra.x = file.ReadSingle();
                    GlassExtra.y = file.ReadSingle();
                    GlassExtra.z = file.ReadSingle();
                    GlassExtra.w = file.ReadSingle();
                }

                if (!MwmHelper.ReadString(file, out GlassCW)) return false;
                if (!MwmHelper.ReadString(file, out GlassCCW)) return false;
                GlassSmoothNormals = file.ReadByte() != 0;
            }
            return true;
        }

        public bool WriteBin(BinaryWriter file, int version)
        {
            if (!MwmHelper.WriteString(file, name)) return false;

            if (version < 1052002)
            {
                file.Write(2);
                if (!MwmHelper.WriteString(file, "DiffuseTexture")) return false;
                if (!MwmHelper.WriteString(file, texpath.Length > 0 ? texpath[0].value : "")) return false;
                if (!MwmHelper.WriteString(file, "NormalTexture")) return false;
                if (!MwmHelper.WriteString(file, texpath.Length > 1 ? texpath[1].value : "")) return false;
            }
            else
            {
                file.Write(texpath.Length);
                foreach (MwmStringParam str in texpath)
                    if (!str.WriteBin(file)) return false;
            }

            file.Write(glossiness);
            file.Write(diffuseColor.x);
            file.Write(diffuseColor.y);
            file.Write(diffuseColor.z);
            file.Write(specularColor.x);
            file.Write(specularColor.y);
            file.Write(specularColor.z);

            if (!MwmHelper.WriteString(file, technique.ToString())) return false;

            if (technique == Technique.GLASS)
            {
                if (version < 1043001)
                {

                }
                else
                {
                    file.Write(GlassExtra.x);
                    file.Write(GlassExtra.y);
                    file.Write(GlassExtra.z);
                    file.Write(GlassExtra.w);
                }

                if (!MwmHelper.WriteString(file, GlassCW)) return false;
                if (!MwmHelper.WriteString(file, GlassCCW)) return false;
                file.Write((byte)(GlassSmoothNormals ? 1 : 0));
            }
            return true;
        }
    }

    public class MwmModelBVH
    {
        string title = "ModelBvh";
        
        public int byte_count = 0;
        public int nodes_count = 0;
        public BoxAA bound;
        public Vector3 quantizationSize;
        public MwmBvhNode[] nodes;


        static void clamp(ref Vector3 v, float min, float max)
        {
            v.x = v.x < min ? min : v.x > max ? max : v.x;
            v.y = v.y < min ? min : v.y > max ? max : v.y;
            v.z = v.z < min ? min : v.z > max ? max : v.z;
        }

        /// <summary>
        /// Generate a new tree using vertices and faces informations, boundingbox are necessary 
        /// </summary>
        public static MwmModelBVH Generate(IList<Vector3> vertices, IList<Face16> triangles, BoxAA boundingbox)
        {
            MwmModelBVH bvh = new MwmModelBVH();


            // int don't know if this is correct
            Vector3 halfsize = boundingbox.HalfSize;
            Vector3 size = halfsize*2;
            if (halfsize.x < 1.0f) halfsize.x = 1.001f;
            if (halfsize.y < 1.0f) halfsize.y = 1.001f;
            if (halfsize.z < 1.0f) halfsize.z = 1.001f;
            
            bvh.bound = BoxAA.FromHalfSize(Vector3.Zero, halfsize);

            bvh.quantizationSize = 32767.0f / halfsize;

            BVHTree algorithm = new BVHTree(vertices, triangles, 6, 20);
            List<BVHNode> nodes = new List<BVHNode>(algorithm);
            
            bvh.nodes = new MwmBvhNode[nodes.Count];
            
            for (int i = 0; i < nodes.Count; i++)
            {
                BVHNode node = nodes[i];
                MwmBvhNode bvhnode = new MwmBvhNode();

                // need to understand this because don't have sense for me
                Vector3 min = (nodes[i].bound.min - bvh.bound.min) * bvh.quantizationSize;
                Vector3 max = (nodes[i].bound.max - bvh.bound.min) * bvh.quantizationSize;

                clamp(ref min, 0, ushort.MaxValue);
                clamp(ref max, 0, ushort.MaxValue);

                bvhnode.quantized = new BoxAA(min, max);

                if (node.IsLeaf)
                {
                    bvhnode.index = new int[node.primitives.Count];
                    for (int j = 0; j < node.primitives.Count; j++) bvhnode.index[j] = node.primitives[j].id;
                }
                else
                {
                    int escapeOffset = node.escapeIndex - node.IndexByRelation;
                    if (escapeOffset > nodes.Count || escapeOffset < 1) throw new ArgumentOutOfRangeException("wrong escape offset calculation");
                    bvhnode.index = new int[] { -escapeOffset };
                }

                bvh.nodes[i] = bvhnode;
            }
            return bvh;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            if (title != "ModelBvh") return false;
            byte_count = file.ReadInt32();

            long current_position = file.BaseStream.Position;

            if (byte_count < 0 || byte_count > 1000000) return false;

            if (byte_count > 0)
            {
                nodes_count = file.ReadInt32();
                if (nodes_count < 0 || nodes_count > 10000) return false;
                bound = new BoxAA(file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle(), file.ReadSingle());

                quantizationSize = new Vector3(file.ReadSingle(), file.ReadSingle(), file.ReadSingle());

                nodes = new MwmBvhNode[nodes_count];
                for (int i = 0; i < nodes_count; i++)
                    if (!nodes[i].ReadBin(file)) return false;
            }

            if (current_position + byte_count != file.BaseStream.Position) return false;

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title)) return false;

            if (nodes != null && nodes.Length > 0)
            {
                uint bytesize = 4 + 4 * 6 + 4 * 3;
                foreach (MwmBvhNode node in nodes)
                    bytesize += node.serializedBytesCount;

                file.Write(bytesize);
                file.Write(nodes.Length);

                file.Write(bound.min.x);
                file.Write(bound.min.y);
                file.Write(bound.min.z);
                file.Write(bound.max.x);
                file.Write(bound.max.y);
                file.Write(bound.max.z);

                file.Write(quantizationSize.x);
                file.Write(quantizationSize.y);
                file.Write(quantizationSize.z);

                foreach (MwmBvhNode node in nodes)
                    if (!node.WriteBin(file)) return false;
            }
            else
            {
                file.Write((uint)0);
            }

            return true;
        }
    }

    public class MwmModelInfo 
    {
        string title = "ModelInfo";
        public int triangles_count = 0; // attention : is sum of triangles of all model parts
        public int vertices_count = 0;
        public Vector3 boundingboxsize = Vector3.Zero;


        public MwmModelInfo()
        {
        }

        public MwmModelInfo(int totVerts, int totFaces, Vector3 size)
        {
            triangles_count = totFaces;
            vertices_count = totVerts;
            boundingboxsize = size;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            if (title != "ModelInfo") return false;
            triangles_count = file.ReadInt32();
            vertices_count = file.ReadInt32();
            boundingboxsize.x = file.ReadSingle();
            boundingboxsize.y = file.ReadSingle();
            boundingboxsize.z = file.ReadSingle();
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title)) return false;
            file.Write(triangles_count);
            file.Write(vertices_count);
            file.Write(boundingboxsize.x);
            file.Write(boundingboxsize.y);
            file.Write(boundingboxsize.z);
            return true;
        }
    }
    
    public class MwmModelBlend
    {
        string title1 = "BlendIndices";
        string title2 = "BlendWeights";

        public int indices_count_4 = 0; // array lenght is x4, maxscript don't like multidimensional or jagger array
        public int weights_count_4 = 0;

        public int[] indices;
        public float[] weights;


        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title1)) return false;
            if (title1 != "BlendIndices") return false;
            indices_count_4 = file.ReadInt32();
            if (indices_count_4 < 0 || indices_count_4 > 1000000) return false;

            indices = new int[indices_count_4 * 4];
            for (int i = 0; i < indices_count_4 * 4; i++) indices[i] = file.ReadInt32();

            if (!MwmHelper.ReadString(file, out title2)) return false;
            if (title2 != "BlendWeights") return false;
            weights_count_4 = file.ReadInt32();
            if (weights_count_4 < 0 || weights_count_4 > 1000000) return false;

            weights = new float[weights_count_4 * 4];
            for (int i = 0; i < weights_count_4 * 4; i++) weights[i] = file.ReadSingle();

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file,title1)) return false;
            if (indices != null)
            {
                file.Write(indices.Length / 4);
                foreach (int idx in indices) file.Write(idx);
            }
            else file.Write(0);

            
            if (!MwmHelper.WriteString(file, title2)) return false;
            if (weights != null)
            {
                file.Write(weights.Length / 4);
                foreach (float w in weights) file.Write(w);
            }
            else file.Write(0);

            return true;
        }
    }

    public class MwmAnimations 
    {
        string title = "Animations";
        public int animations_count = 0;
        public int key_count = 0;
        public MwmAnimationClip[] animations;
        public int[] keys;

        public static MwmAnimations Default(int numbones)
        {
            MwmAnimations anim = new MwmAnimations();

            anim.keys = new int[numbones];
            for (int i = 0; i < numbones; i++) anim.keys[i] = i;

            return anim;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            if (title != "Animations") return false;
            animations_count = file.ReadInt32();
            if (animations_count < 0 || animations_count > 1000) return false;

            animations = new MwmAnimationClip[animations_count];

            for (int i = 0; i < animations_count; i++)
            {
                animations[i] = new MwmAnimationClip();
                if (!animations[i].ReadBin(file)) return false;
            }

            key_count = file.ReadInt32();       
            if (key_count < 0 || key_count > 1000) return false;

            keys = new int[key_count];
            for (int i = 0; i < key_count; i++) keys[i] = file.ReadInt32();

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {

            if (!MwmHelper.WriteString(file, title)) return false;

            if (animations != null)
            {
                file.Write(animations.Length);
                foreach (MwmAnimationClip anim in animations)
                    if (!anim.WriteBin(file)) return false;
            }
            else file.Write(0);

            if (keys != null)
            {
                file.Write(keys.Length);
                foreach (int k in keys) file.Write(k);
            }
            else file.Write(0);

            return true;
        }
    }

    public class MwmAnimationClip
    {
        public string name = "";
        public float duration = 0.0f;
        public int bones_count = 0;
        public MwmAnimBone[] bones;

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;
            duration = (float)file.ReadDouble();
            bones_count = file.ReadInt32();
            if (bones_count < 0 || bones_count > 1000) return false;

            bones = new MwmAnimBone[bones_count];

            for (int i = 0; i < bones_count; i++)
            {
                bones[i] = new MwmAnimBone();
                if (!bones[i].ReadBin(file)) return false;
            }
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, name)) return false;
            file.Write((double)duration);
            file.Write(bones.Length);
            foreach (MwmAnimBone bone in bones) if (!bone.WriteBin(file)) return false;
            return true;
        }
    }


    public class MwmAnimBone
    {
        public string name = "";
        public int keys_count = 0;
        public MwmKeyframe[] keys;


        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;

            keys_count = file.ReadInt32();
            if (keys_count < 0 || keys_count > 1000) return false;

            keys = new MwmKeyframe[keys_count];
            for (int i = 0; i < keys_count; i++)
            {
                keys[i] = new MwmKeyframe();
                if (!keys[i].ReadBin(file)) return false;
            }
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, name)) return false;
            file.Write(keys.Length);
           
            foreach (MwmKeyframe k in keys)
                if (!k.WriteBin(file)) return false;
            
            return true;
        }
    }

    public class MwmSkeleton
    {
        string title1 = "Bones";
        string title2 = "BoneMapping";
        public int bones_count = 0;
        public int mapping_count = 0;
        public MwmBone[] bones;
        public int[] mapping;

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title1)) return false;
            if (title1 != "Bones") return false;
            bones_count = file.ReadInt32();
            if (bones_count < 0 || bones_count > 1000) return false;

            bones = new MwmBone[bones_count];
            for (int i = 0; i < bones_count; i++)
            {
                bones[i] = new MwmBone();
                if (!bones[i].ReadBin(file)) return false;
            }

            if (!MwmHelper.ReadString(file, out title2)) return false;
            if (title2 != "BoneMapping") return false;

            mapping_count = file.ReadInt32();
            if (mapping_count < 0 || mapping_count > 1000) return false;
            
            mapping = new int[mapping_count];
            for (int i = 0; i < mapping_count; i++) mapping[i] = file.ReadInt32();

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title1)) return false;

            if (bones != null)
            {
                file.Write(bones.Length);
                foreach (MwmBone bone in bones) if (!bone.WriteBin(file)) return false;
            }
            else file.Write(0);
            
            if (!MwmHelper.WriteString(file, title2)) return false;
            if (mapping != null)
            {
                file.Write(mapping.Length);
                foreach (int idx in mapping) file.Write(idx);
            }
            else file.Write(0);

            return true;
        }
    }
   
    public class MwmBone 
    {
        public string name = "";
        public int hierarchy = 0;
        public Matrix4 transform = Matrix4.Identity;

        public static MwmBone RootBone()
        {
            return new MwmBone{name = "RootBone" , hierarchy = -1 , transform = Matrix4.Identity};
        }

        public MwmBone()
        {
        }
        public MwmBone(string name, Matrix4 transform, int hierarchy)
        {
            this.name = name;
            this.transform = transform;
            this.hierarchy = hierarchy;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out name)) return false;
            hierarchy = file.ReadInt32();
            if (!MwmHelper.ReadMatrix(file, out transform)) return false;
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, name)) return false;
            file.Write(hierarchy);
            if (!MwmHelper.WriteMatrix(file, transform)) return false;
            return true;
        }

    }

    public class MwmHavok 
    {
        string title = "HavokCollisionGeometry";
        public int data_count = 0;
        public byte[] data;

        /// <summary>
        /// can use a precompiled instance
        /// </summary>
        public static MwmHavok Default()
        {
            return new MwmHavok();
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title)) return false;
            if (title != "HavokCollisionGeometry") return false;
            data_count = file.ReadInt32();
            if (data_count < 0 || data_count > 1000000) return false;
            data = file.ReadBytes(data_count);
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title)) return false;
            if (data != null)
            {
                file.Write(data.Length);
                file.Write(data);
            }
            else
            {
                file.Write(0);
            }
            return true;
        }
    }

    public class MwmLods
    {
        string title1 = "PatternScale";
        string title2 = "LODs";
        
        public float PatternScale = 1.0f;
        public int lods_count = 0;
        public float[] lods_distance;
        public string[] lods_path;

        /// <summary>
        /// can use a precompiled instance
        /// </summary>
        public MwmLods()
        {
            PatternScale = 1.0f;
        }

        public bool ReadBin(BinaryReader file)
        {
            if (!MwmHelper.ReadString(file, out title1)) return false;
            if (title1 != "PatternScale") return false;
            PatternScale = file.ReadSingle();

            if (!MwmHelper.ReadString(file, out title2)) return false;
            if (title2 != "LODs") return false;
            lods_count = file.ReadInt32();
            if (lods_count < 0 || lods_count > 1000000) return false;

            lods_distance = new float[lods_count];
            lods_path = new string[lods_count];

            for (int i = 0; i < lods_count; i++)
            {
                lods_distance[i] = file.ReadSingle();
                if (!MwmHelper.ReadString(file, out lods_path[i])) return false;
            }

            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            if (!MwmHelper.WriteString(file, title1)) return false;
            file.Write(PatternScale);

            if (!MwmHelper.WriteString(file, title2)) return false;

            if (lods_distance != null || lods_path != null)
            {
                file.Write(lods_distance.Length);
                for (int i = 0; i < lods_distance.Length; i++)
                {
                    file.Write(lods_distance[i]);
                    if (!MwmHelper.WriteString(file, lods_path[i])) return false;
                }
            }
            else
            {
                file.Write(0);
            }
            return true;
        }
    }
}
