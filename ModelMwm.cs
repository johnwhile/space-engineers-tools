﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.Serialization;

using Engine.Maths;
using Engine.Graphics;

namespace SpaceEngineers
{
    public enum CoordinateSystem
    {
        Directx = 0, //left handle
        OpenGL,
        Max3DStudio,
        Blender
    }

    /// <summary>
    /// The Main Class to manage the mwm structure, compiled in framework 2.0 and checked in maxscipt2014 dotnet, i suppose can work
    /// also for previous version of 3dstudio.
    /// Maxscript support:
    /// Int , Uint , Ushort, Short, Byte , String and Array of these.
    /// I store separatly the lenght of array to avoid null reference exeption.
    /// Class must not generate exeptions.
    /// Trace can not be used
    /// const value aren't recognized, use public static
    /// </summary>
    public class ModelMwm
    {
        CoordinateSystem coordsys;
        int version = 01052002;

        /// <summary>
        /// Tested in maxscript :
        /// 3DStransform = CoordMatrix * D3DXtransform * InvCoordMatrix
        /// D3DXtransform = InvCoordMatrix * 3DStransform * CoordMatrix
        /// 
        /// </summary>
        public Matrix4 CoordMatrix, InvCoordMatrix;

        public CoordinateSystem CoordSystem
        {
            get { return coordsys; }
        }

        public string debugmessage = "ok";

        public MwmHeader header;
        public MwmDummies dummies;
        public MwmVertices vertices;
        public MwmModelParams modelparams;
        public MwmParts modelparts;
        public MwmModelBVH modelbvh;
        public MwmModelInfo modelinfo;
        public MwmModelBlend modelblend;
        public MwmAnimations animations;
        public MwmSkeleton skeleton;
        public MwmHavok havok;
        public MwmLods lods;


        public ModelMwm()
            : this(CoordinateSystem.Directx)
        {

        }
        /// <summary>
        /// Initialized a new Mwm model file, this generate default values foreach structures.
        /// The data are read and write in CoordinateSystem.Directx, but save in the structures with "coordsys"
        /// </summary>
        /// <param name="CoordSystem">The data will be converted from Directx to this coordinate system</param>
        public ModelMwm(CoordinateSystem CoordSystem)
        {
            coordsys = CoordSystem;

            switch (coordsys)
            {
                case CoordinateSystem.Max3DStudio:
                    CoordMatrix = new Matrix4(
                        1, 0, 0, 0,
                        0, 0, -1, 0,
                        0, 1, 0, 0,
                        0, 0, 0, 1);

                    InvCoordMatrix = CoordMatrix.Inverse();
                    break;

                default: CoordMatrix = InvCoordMatrix = Matrix4.Identity; break;
            }

            header = new MwmHeader();
            dummies = new MwmDummies();
            vertices = new MwmVertices();
            modelparams = new MwmModelParams();
            modelparts = new MwmParts();
            modelbvh = new MwmModelBVH();
            modelinfo = new MwmModelInfo();
            modelblend = new MwmModelBlend();
            animations = new MwmAnimations();
            skeleton = new MwmSkeleton();
            havok = new MwmHavok();
            lods = new MwmLods();
        }



        /// <summary>
        /// By default is 01052002
        /// </summary>
        public int MwmVersion
        {
            get { return version; }
            set { version = value; }
        }


        /// <summary>
        /// Initialized a new Mwm model file with basic geometry information, the TBN matrix and BVH structure 
        /// recalculated by scrach and i'm try to make something that work also in maxscript.
        /// you need to initialize the missing structures
        /// </summary>
        /// <param name="mesh">require num of triangles foreach meshes</param>
        public bool InitializeGeometry(Vector3[] verts, Vector2[] texcoords, Vector3[] normals, Face16[] triangles, int[] meshes, MwmModelMaterial[] materials)
        {
            int numMeshes = meshes.Length;
            int numTotFaces = triangles.Length;
            int numVerts = verts.Length;

            debugmessage = verts[0].ToString();

            if (materials.Length != numMeshes || texcoords.Length != numVerts || normals.Length != numVerts)
            {
                debugmessage = "num or count wrong";
                return false;
            }

            ///////////////////////////////////////////////////////////
            vertices = MwmVertices.Generate(verts, texcoords, normals, triangles);

            /////////////////////////////////////////////////////////// 
            modelparams = MwmModelParams.Default();
            modelparams.BoundingBox = BoxAA.FromData(verts);
            modelparams.BoundingSphere = Sphere.FromDataFast(verts);


            /////////////////////////////////////////////////////////// 
            modelparts = new MwmParts();
            modelparts.parts = new MwmPart[numMeshes];
            int offset = 0;
            for (int n = 0; n < numMeshes; n++)
            {
                MwmPart part = new MwmPart();
                int count = meshes[n];
                if (offset + count > numTotFaces)
                {
                    debugmessage = "num or face wrong";
                    return false;
                }
                part.triangles = new Face16[count];
                for (int i = 0; i < count; i++)
                    part.triangles[i] = triangles[i + offset];

                part.hasMaterial = materials[n] != null;
                part.material = materials[n];
                part.hash = part.GetHashCode();

                modelparts.parts[n] = part;
                offset += count;
            }

            ///////////////////////////////////////////////////////////
            modelbvh = MwmModelBVH.Generate(verts, triangles, modelparams.BoundingBox);

            ///////////////////////////////////////////////////////////
            modelinfo = new MwmModelInfo(numVerts, numTotFaces, modelparams.BoundingBox.HalfSize * 2);

            return true;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    bool complete = ReadBin(file);
                    filepos = complete ? -1 : file.BaseStream.Position;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                }
            }
            return filepos;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 SaveFile(string filepath)
        {
            Int64 filepos = -1;

            try
            {
                FileStream stream = new FileStream(filepath, FileMode.Create, FileAccess.Write);

                using (BinaryWriter file = new BinaryWriter(stream))
                {
                    try
                    {
                        bool complete = WriteBin(file);
                        filepos = complete ? -1 : file.BaseStream.Position;
                    }
                    catch (Exception e)
                    {
                        filepos = file.BaseStream.Position;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        
            return filepos;
        }

        /// <summary>
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            file.BaseStream.Seek(0, SeekOrigin.Begin);

            if (!header.ReadBin(file)) { debugmessage = " header fail"; return false; }
            version = header.version;
            if (!dummies.ReadBin(file)) { debugmessage = " dummies fail"; return false; }
            if (!vertices.ReadBin(file)) { debugmessage = " vertices fail"; return false; }
            if (!modelparams.ReadBin(file)) { debugmessage = " modelparams fail"; return false; }
            if (!modelparts.ReadBin(file, header.version)) { debugmessage = " modelparts fail"; return false; }
            if (!modelbvh.ReadBin(file)) { debugmessage = " modelbvh fail"; return false; }
            if (!modelinfo.ReadBin(file)) { debugmessage = " modelinfo fail"; return false; }
            if (!modelblend.ReadBin(file)) { debugmessage = " modelblend fail"; return false; }
            if (!animations.ReadBin(file)) { debugmessage = " animations fail"; return false; }
            if (!skeleton.ReadBin(file)) { debugmessage = " skeleton fail"; return false; }
            if (!havok.ReadBin(file)) { debugmessage = " havok fail"; return false; }
            if (!lods.ReadBin(file)) { debugmessage = " lods fail"; return false; }
            return true;

        }

        /// <summary>
        /// Try to write all structures (usefull for debug) but return false is something wrong
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            bool incomplete = false;

            try
            {
                if (!header.WriteBin(file, version)) return false;
                if (!dummies.WriteBin(file)) incomplete |= true;
                if (!vertices.WriteBin(file)) incomplete |= true;
                if (!modelparams.WriteBin(file)) incomplete |= true;
                if (!modelparts.WriteBin(file, version)) incomplete |= true;
                if (!modelbvh.WriteBin(file)) incomplete |= true;
                if (!modelinfo.WriteBin(file)) incomplete |= true;
                if (!modelblend.WriteBin(file)) incomplete |= true;
                if (!animations.WriteBin(file)) incomplete |= true;
                if (!skeleton.WriteBin(file)) incomplete |= true;
                if (!havok.WriteBin(file)) incomplete |= true;
                if (!lods.WriteBin(file)) incomplete |= true;
            }
            catch (Exception e)
            {
                incomplete |= true;
                debugmessage += e.Message.ToString();
            }
            return !incomplete;
        }
    }
    


}

