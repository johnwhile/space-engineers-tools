This folder contain this git repository: 
https://bitbucket.org/johnwhile/space-engineers-tools/src
	
	
CommonLib.dll

It's used in one of my learning project for SharpDX9, contain only maths methods, you can implement your own, is related to this git repository :
https://bitbucket.org/johnwhile/directx9-with-sharpdx/src/3f9caf61a24547933cdcc676e2f982389a98f9d9/CommonLib/?at=master

Eventually remember to change manually the "SElib.csproj" and/or "SEtest.csproj" and add these line to target the correct assembly version:

  <!-- x64 specific references -->
  <ItemGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x64' ">
    <Reference Include="CommonLib">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\CommonLib\bin\x64\Release\CommonLib.dll</HintPath>
    </Reference>
  </ItemGroup>
  
   <ItemGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x64' ">
    <Reference Include="CommonLib">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\CommonLib\bin\x64\Debug\CommonLib.dll</HintPath>
    </Reference>
  </ItemGroup>

  <!-- x86 specific references -->
  <ItemGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x86' ">
    <Reference Include="CommonLib">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\CommonLib\bin\x86\Release\CommonLib.dll</HintPath>
    </Reference>
  </ItemGroup> 
    
   <ItemGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x86' ">
    <Reference Include="CommonLib">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\CommonLib\bin\x86\Debug\CommonLib.dll</HintPath>
    </Reference>
  </ItemGroup>

This because VisualStudio (microsoft...) don't have a command in interface to do this, and can delete these conditions.





All these repository are completely free to use, without restrictions, can be used and uploaded by everyone want fix or add new functions.
