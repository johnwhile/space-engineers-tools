﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using SpaceEngineers;
using Engine.Maths;
using Engine.Graphics;

namespace SEtest
{
    public partial class Form1 : Form
    {
        ModelMwm model = null;
        string history = "";

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public Form1()
        {
            InitializeComponent();
            DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            history = dir.Parent.Parent.Parent.Parent.Parent.FullName.ToString() + @"\examples";
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = history;
            openFileDialog1.Filter = "SEmodel (*.mwm)|*.mwm|all (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            history = openFileDialog1.FileName;
            if (result == DialogResult.OK)
            {
                model = new ModelMwm();
                long filepos = model.OpenFile(openFileDialog1.FileName);

                if (filepos > -1)
                {
                    Console.WriteLine("ERROR " + model.debugmessage);
                }
                else
                {
                    this.treeView1.Nodes.Clear();

                    
                    List<Face16> triangles = new List<Face16>();
                    foreach (MwmPart part in model.modelparts.parts)
                        triangles.AddRange(part.triangles);

                    BVHTree algorithm = new BVHTree(model.vertices.position, triangles, 6, 20);
                    this.treeView1.Nodes.Add(algorithm.root.GetTreeRappresentation());
                    this.treeView1.Invalidate();

                    BVHNode[] array = algorithm.ToArray();
                    this.listBox1.Items.AddRange(array);
                    this.listBox1.Invalidate();
                }

                Console.WriteLine("READING COMPLETE");
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (model != null)
            {
                saveFileDialog1.InitialDirectory = history;
                saveFileDialog1.Filter = "SEmodel (*.mwm)|*.mwm|all (*.*)|*.*";
                DialogResult result = saveFileDialog1.ShowDialog();
                history = saveFileDialog1.FileName;
                
                if (result == DialogResult.OK)
                {
                    if (model.modelinfo.vertices_count != 0)
                    {
                        List<Face16> triangles = new List<Face16>();

                        foreach (MwmPart part in model.modelparts.parts)
                            triangles.AddRange(part.triangles);

                        model.modelparams.BoundingBox = BoxAA.FromData(model.vertices.position);
                        model.modelparams.BoundingSphere = Sphere.FromDataFast(model.vertices.position);

                        model.modelinfo = new MwmModelInfo(model.vertices.position.Length, triangles.Count, model.modelparams.BoundingBox.HalfSize * 2);
                        model.vertices = MwmVertices.Generate(model.vertices.position, model.vertices.texcood, model.vertices.normal, triangles);
                        model.modelbvh = MwmModelBVH.Generate(model.vertices.position, triangles, model.modelparams.BoundingBox);
                    }
                    else
                    {
                        Console.WriteLine("animation version");
                    }
                    long filepos = model.SaveFile(saveFileDialog1.FileName);

                    if (filepos > -1)
                    {
                        Console.WriteLine("ERROR");
                    }
                    Console.WriteLine("WRITING COMPLETE");
                }
            }
        }
    }
}
